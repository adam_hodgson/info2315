<!DOCTYPE html>
<html>
    <head>
        <title>The Pentagon</title>
        <link rel="stylesheet" type="text/css" href="style.css" \>
    </head>

    <body>
<?php 

$file = 'secretdata.txt';

if (file_exists($file)) {
	$data = trim(file_get_contents($file));
}

if (! isset($_POST["pass"])) { ?>

        <h1>The Pentagon</h1>

        <img id="matrix" src="http://i987.photobucket.com/albums/ae354/spartts/Matrix.gif" border="0" alt="the matrix gif photo: matrix Matrix.gif"/>

        <h2>Log in to find access to top secret documents which contain vital information in regards to your current mission.</h2>

        <div id="login">
            <form action="pentagon.php" method="post">
                Username: <input name="user" type="text" \><br>
                Password: <input name="pass" type="password" \><br>
                <input type="submit" value="Login">
                <!-- note to self: saved password in pass.txt -->
            </form>
        </div>

<?php } else if ($_POST["pass"] == "matrix256") { ?>

        <h1>Mission Alpha</h1>

        <img id="matrix" src="http://i987.photobucket.com/albums/ae354/spartts/Matrix.gif" border="0" alt="the matrix gif photo: matrix Matrix.gif"/>

        <p>Welcome agent <b><?php echo $_POST["user"]; ?></b><br>

        <p><?php echo $data ?></p>

<?php } else {?>

        <h1>Access Denied</h1>

        <img id="matrix" src="http://i987.photobucket.com/albums/ae354/spartts/Matrix.gif" border="0" alt="the matrix gif photo: matrix Matrix.gif"/>

        <p>There is no account with username: <b><?php echo $_POST["user"]; ?></b>, and the password provided.<br>A record of this login attmept has been created</p>

<?php } ?>
    </body>
</html>
